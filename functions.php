<?php
/**
 * Describe child theme functions
 *
 * @package SparkleStore
 * @subpackage Sparkle Store
 * 
 */

/*-------------------------------------------------------------------------------------------------------------------------------*/
if ( ! function_exists( 'sparkle_store_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function sparkle_store_setup() {
    add_theme_support( "title-tag" );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( "wp-block-styles" );
    add_theme_support( "responsive-embeds" );
    add_theme_support( "align-wide" );
    
    $sparklestore_lite_theme_info = wp_get_theme();
    $GLOBALS['sparklestore_lite_version'] = $sparklestore_lite_theme_info->get( 'Version' );
}
endif;

add_action( 'after_setup_theme', 'sparkle_store_setup' );


/**
 * Register Google fonts for News Portal Lite.
 *
 * @return string Google fonts URL for the theme.
 * @since 1.0.0
 */
if ( ! function_exists( 'sparkle_store_fonts_url' ) ) :
    function sparkle_store_fonts_url() {

        $fonts_url = '';
        $font_families = array();

        /*
         * Translators: If there are characters in your language that are not supported
         * by Raleway, translate this to 'off'. Do not translate into your own language.
         */
        if ( 'off' !== _x( 'on', 'Oswald font: on or off', 'sparkle-store' ) ) {
            $font_families[] = 'Oswald:100,200,200i,300,400,500,600,700,800';
        }
        
        if ( 'off' !== _x( 'on', 'Inter font: on or off', 'sparkle-store' ) ) {
            $font_families[] = 'Inter:100,200,200i,300,400,500,600,700,800';
        }

        if( $font_families ) {
            $query_args = array(
                'family' => urlencode( implode( '|', $font_families ) ),
                'subset' => urlencode( 'latin,latin-ext' ),
            );

            $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
        }

        return $fonts_url;
    }
endif;

/**
 * Enqueue child theme styles and scripts
*/
add_action( 'wp_enqueue_scripts', 'sparkle_store_scripts', 20 );

function sparkle_store_scripts() {
    
    global $sparklestore_lite_version;

    wp_enqueue_style( 'sparkle-store-google-font', sparkle_store_fonts_url(), array(), null );
    
    wp_dequeue_style( 'sparklestore-style' );
    wp_dequeue_style( 'sparklestore-style-responsive' );
    
	wp_enqueue_style( 'sparkle-store-parent-style', trailingslashit( esc_url ( get_template_directory_uri() ) ) . '/style.css', array(), esc_attr( $sparklestore_lite_version ) );
    wp_enqueue_style( 'sparkle-store-style', get_stylesheet_uri(), array(), esc_attr( $sparklestore_lite_version ) );
    wp_enqueue_style( 'sparklestore-style-responsive');
    
}


add_action( 'wp_head', 'remove_my_action' );
function remove_my_action() {
    remove_action( 'sparklestore_header', 'sparklestore_main_header', 20 );
}

/**
 * Main Header Area
*/
if ( ! function_exists( 'sparkle_store_main_header' ) ) {
	
	function sparkle_store_main_header() { ?>

		<div class="mainheader mobile-only">
			<div class="container">
				<div class="header-middle-inner">
					<?php 
						/**
						 * Menu Toggle
						*/
						do_action('sparklestore_menu_toggle'); 
					?>

			        <div class="sparklelogo">
		              	<?php 
		              		if ( function_exists( 'the_custom_logo' ) ) {
								the_custom_logo();
							} 
						?>
		              	<div class="site-branding">				              		
		              		<h1 class="site-title">
		              			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
		              				<?php bloginfo( 'name' ); ?>
		              			</a>
		              		</h1>
		              		<?php
		              		$description = get_bloginfo( 'description', 'display' );
		              		if ( $description || is_customize_preview() ) { ?>
		              			<p class="site-description"><?php echo $description; ?></p>
		              		<?php } ?>
		              	</div>
			        </div><!-- End Header Logo --> 

			        <div class="rightheaderwrapend">
	    	          	<?php if ( sparklestore_is_woocommerce_activated() ) {  ?>
	    	          		<div id="site-header-cart" class="site-header-cart block-minicart sparkle-column">
								<?php echo wp_kses_post( sparklestore_shopping_cart() ); ?>
					            <div class="shopcart-description">
									<?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
					            </div>
					        </div>
	    		        <?php } ?> 
			        </div>
			    </div>

			    <div class="rightheaderwrap">
		        	<div class="category-search-form">
		        	  	<?php 
		        	  		/**
		        	  		 * Normal & Advance Search
		        	  		*/
		        	  		if ( sparklestore_is_woocommerce_activated() ) {

		        	  			sparklestore_advance_product_search_form();
		        	  		}
		        	  	?>
		        	</div>
		        </div>
		        
			</div>
		</div>


		<div class="mainheader header-nav">
			<div class="container">
				<div class="header-middle-inner">
			        <div class="sparklelogo">
		              	<?php 
		              		if ( function_exists( 'the_custom_logo' ) ) {
								the_custom_logo();
							} 
						?>
		              	<div class="site-branding">				              		
		              		<h1 class="site-title">
		              			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
		              				<?php bloginfo( 'name' ); ?>
		              			</a>
		              		</h1>
		              		<?php
		              		$description = get_bloginfo( 'description', 'display' );
		              		if ( $description || is_customize_preview() ) { ?>
		              			<p class="site-description"><?php echo $description; ?></p>
		              		<?php } ?>
		              	</div>
			        </div><!-- End Header Logo --> 

                    
                        
                    <div class="header-nav-inner">
                        <?php do_action( 'sparklestore_header_vertical' ); ?>
                        
                        <div class="box-header-nav main-menu-wapper">
                            <div class="main-menu sp-clearfix">
                                <div class="main-menu-links">
                                    <?php
                                        wp_nav_menu( array(
                                                'theme_location'  => 'sparkleprimary',
                                                'menu'            => 'primary-menu',
                                                'container'       => '',
                                                'container_class' => '',
                                                'container_id'    => '',
                                                'menu_class'      => 'main-menu',
                                            )
                                        );
                                    ?>
                                </div>
                            </div>


                            <div class="category-search-form">
                                <?php 
                                    /**
                                     * Normal & Advance Search
                                    */
                                    if ( sparklestore_is_woocommerce_activated() ) {

                                        sparklestore_advance_product_search_form();
                                    }else{ 
                                        $searchplaceholder = get_theme_mod('sparklestore_search_placeholder_text','I&#39;m searching for...' ); 
                                        ?>
                                        <div class='block-search'>
                                            <form role="product-search" method="get" action="/" class="form-search block-search advancesearch">
                                                <input type="hidden" name="post_type" value="post"/>
                                                <div class="form-content search-box results-search">
                                                    <div class="inner">
                                                        <input autocomplete="off" type="text" class="input searchfield txt-livesearch" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" placeholder="<?php echo esc_attr( $searchplaceholder ); ?>">
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn-submit">
                                                    <span class="fa fa-search" aria-hidden="true"></span>
                                                </button>
                                            </form>
                                        </div>
                                        <?php
                                    }
                                ?>

                                <a href="javascript:void(0)" class="search-layout-two sparkle-close-icon">
                                    <span>x</span>
                                </a>

                            </div>
                        </div>
                    </div>

			    </div>
			</div>
		</div>		    
		<?php
	}
}
add_action( 'sparklestore_header', 'sparkle_store_main_header', 20 );

function sparkle_store_add_last_nav_item($items, $args) {
    global $woocommerce;

	if($args->theme_location == 'sparkleprimary'):
		$items .= '<li class="menu-item-search menu-item"><a class="searchicon" href="javascript:void(0)"><i class="fas fa-search"></i></a></li>';
		
		if ( sparklestore_is_woocommerce_activated() ):
            $items .= '<li class="menu-item-sidebar menu-item">';
            $items .= '<div class="site-cart-items-wrap">
                            <div class="cart-icon icofont-cart-alt"></div>
                            <span class="count">'.intval( WC()->cart->cart_contents_count ).'</span>
                            <span class="item">'.wp_kses_post( $woocommerce->cart->get_cart_subtotal() ).'</span>
                        </div>';
            $items .="</li>";
        
        endif;
	endif;
	return $items;
		

}
add_filter('wp_nav_menu_items','sparkle_store_add_last_nav_item', 10, 2);


add_filter( 'sparklestore-style-dynamic-css', 'sparkle_store_dymanic_styles', 100 );
function sparkle_store_dymanic_styles($dynamic_css) {
    $primary_color = get_theme_mod('sparklestore_primary_theme_color_options', '#003772');
    $dynamic_css .="
    .box-header-nav .main-menu .children>.page_item>a, .box-header-nav .main-menu .sub-menu>.menu-item>a{
        background-color: {$primary_color};
        color: #fff;
    ";
    wp_add_inline_style( 'sparkle-store-style', $dynamic_css );
}