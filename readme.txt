=== Sparkle Store ===
Contributors: sparklewpthemes
Tags: one-column, two-columns, right-sidebar, left-sidebar, custom-header, custom-background, custom-menu, translation-ready, featured-images, theme-options, custom-logo, e-commerce, footer-widgets
Requires at least: 5.2
Tested up to: 6.2
Requires PHP: 7.0
Stable tag: 1.0.1
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Sparkle Store is a spotless easy to understand and highlight rich best free eCommerce Sparkle Store kid WordPress topics. The subjects planned and grew particularly for web based shopping, eCommerce store sites. Shimmer store Lite is an adaptable and profoundly adjustable Free Sparkle store Child WordPress subjects that permit you to cause your own novel and expert site you to have for a long time needed. Shimmer store Lite is one of the most open eCommerce online store WordPress subjects which can undoubtedly oblige all kind of clients with no coding abilities to cutting edge or typical web designers.

== Description ==

Sparkle Store is a spotless easy to understand and highlight rich best free eCommerce Sparkle Store kid WordPress topics. The subjects planned and grew particularly for web based shopping, eCommerce store sites. Shimmer store Lite is an adaptable and profoundly adjustable Free Sparkle store Child WordPress subjects that permit you to cause your own novel and expert site you to have for a long time needed. Shimmer store Lite is one of the most open eCommerce online store WordPress subjects which can undoubtedly oblige all kind of clients with no coding abilities to cutting edge or typical web designers. If you face any problem while using our theme, you can refer to our theme documentation (http://docs.sparklewpthemes.com/sparklestore/) or contact our friendly support team (https://sparklewpthemes.com/support/) or Check demo at http://demo.sparklewpthemes.com/sparklestore/sparkle-store/ and Read theme details at https://sparklewpthemes.com/wordpress-themes/sparkle-store


== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports WooCommerce and some other external plugins like YITH WooCommerce Wishlist, YITH WooCommerce Quick View, WOOF – Products Filter for WooCommerce, WooCommerce Variation Swatches, Jetpack, Contact Form 7 and many more plugins.

= Where can I find theme all features ? =

You can check our Theme features at https://demo.sparklewpthemes.com/sparklestore/sparkle-store/.

= Where can I find theme demo? =

You can check our Theme Demo at https://sparklewpthemes.com/wordpress-themes/sparklestore/sparkle-store/


== Translation ==

Sparkle Store theme is translation ready.


== Copyright ==

Sparkle Store WordPress Theme is child theme of SparkleStore, Copyright 2017 Sparkle Themes.
Sparkle Store is distributed under the terms of the GNU GPL

SparkleStore WordPress Theme, Copyright (C) 2018 Sparkle Themes.
SparkleStore is distributed under the terms of the GNU GPL

== Credits ==

	Images used in screenshot

	* Products Image - https://pxhere.com/en/photo/603128
	License: CCO (https://pxhere.com/en/license)

	* Products Image - https://pxhere.com/en/photo/1260533
	License: CCO (https://pxhere.com/en/license)

	* Products Image - https://pxhere.com/en/photo/1584557
	License: CCO (https://pxhere.com/en/license)

	* Products Image - https://pxhere.com/en/photo/1428387
	License: CCO (https://pxhere.com/en/license)


== Changelog ==
= 1.0.1 4th April 2023 =
* WordPress 6.2 Compatible

= 1.0.0 4th April 2022 =

 ** Initial submit theme on wordpress.org trac.